import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
export interface datetime{
  responseArr:String[]
}

@Injectable({
  providedIn: 'root'
})
export class GetTimeServiceService {

  constructor(private http: HttpClient) { }
 
  getReservedTimes(selectedDate) {
    let promise = new Promise((resolve, reject) => {
      this.http.get('http://localhost:3000/getAppointments',{params:{date:selectedDate.year+'-'+selectedDate.month+'-'+selectedDate.day}})
        .toPromise()
        .then(
          (res:datetime) => { 
            console.log(res.responseArr);
            resolve(res.responseArr);
          },
          msg => { 
            reject(msg);
            }
        );
    });
    return promise;
   }
   setReservationDateTime(selectedDate,name,selectedtime){
     let model={
       name:name,
       date:selectedDate.year+'-'+selectedDate.month+'-'+selectedDate.day,
       time:selectedtime
     }
     console.log(model);
    let headers = new HttpHeaders({ 'Content-Type': 'application/JSON' });
    return this.http.post('http://localhost:3000/setAppointment', model,{headers:headers})

   }
}
