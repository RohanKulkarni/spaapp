import { TestBed } from '@angular/core/testing';

import { GetTimeServiceService } from './get-time-service.service';

describe('GetTimeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetTimeServiceService = TestBed.get(GetTimeServiceService);
    expect(service).toBeTruthy();
  });
});
