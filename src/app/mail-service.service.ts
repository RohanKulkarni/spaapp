import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MailServiceService {

  constructor(private http: HttpClient) { }
  sendEmail(toMail,subjectMail,bodyMail){
    let headers = new HttpHeaders({ 'Content-Type': 'application/JSON' });
    let model={
      to:toMail,
      subject:subjectMail,
      body:bodyMail
    }
    return this.http.post('http://localhost:3000/sendEmail', model,{headers:headers})
  }
}
