import { Component, OnInit, ViewChild, HostListener, ElementRef, Input,ViewEncapsulation } from '@angular/core';
import AppointmentPicker from 'appointment-picker';
import {GetTimeServiceService} from '../get-time-service.service'
import { MailServiceService } from '../mail-service.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClientComponent implements OnInit {
  @Input() time : String;
  toMail:String;
  subject:String;
  body:String
  @ViewChild('time',{static: true}) input:ElementRef; 
  picker: AppointmentPicker;
  today = new Date()
  selectedDate={
    day:this.today.getDate(),
    year:this.today.getFullYear(),
    month:this.today.getMonth()+1
  };
  @HostListener('change.appo.picker', ['$event'])
  onChangeTime(event: any) {
    console.log('change.appo.picker', event.time);
  }
  constructor(private getTime: GetTimeServiceService , private mail:MailServiceService) { }
  timesReserved : String[] = []
  ngOnInit() {
    this.getTime.getReservedTimes(this.selectedDate).then(data => {
        this.picker=new AppointmentPicker(this.input.nativeElement,{
          title:"Select A time",
          interval: 30,
          minTime: 9,
          maxTime: 20,
          startTime: 8,
          endTime: 24,
          disabled: data
        });
    });

  }
  onClick() {
    this.picker.close();
   }
 onDateSelect(){
   this.picker.destroy();
    this.getTime.getReservedTimes(this.selectedDate).then(data => {
    this.picker=new AppointmentPicker(this.input.nativeElement,{
      title:"Select A time",
      interval: 30,
      minTime: 9,
      maxTime: 20,
      startTime: 8,
      endTime: 24,
      disabled: data
    });
  });
 }
 save(){
  let model=this.picker.getTime()
   this.getTime.setReservationDateTime(this.selectedDate,'Rohan',model.displayTime).subscribe()
 }
 sendEmail(){
  this.mail.sendEmail(this.toMail,this.subject,this.body).subscribe()
 }
}
